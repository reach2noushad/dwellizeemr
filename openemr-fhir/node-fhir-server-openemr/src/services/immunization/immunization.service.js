/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT } = require('../../constants');

let getImmunization = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.IMMUNIZATION));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> search');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let date = args['date'];
	let dose_sequence = args['dose-sequence'];
	let identifier = args['identifier'];
	let location = args['location'];
	let lot_number = args['lot-number'];
	let manufacturer = args['manufacturer'];
	let notgiven = args['notgiven'];
	let patient = args['patient'];
	let practitioner = args['practitioner'];
	let reaction = args['reaction'];
	let reaction_date = args['reaction-date'];
	let reason = args['reason'];
	let reason_not_given = args['reason-not-given'];
	let status = args['status'];
	let vaccine_code = args['vaccine-code'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Immunization = getImmunization(base_version);

	// Cast all results to Immunization Class
	let immunization_resource = new Immunization();
	// TODO: Set data with constructor or setter methods
	immunization_resource.id = 'test id';

	// Return Array
	resolve([immunization_resource]);
});

function getImmunizationWithIdQuery(immunizationId) {
	let query = `SELECT imz.id,
		imz.patient_id,
		imz.administered_date,
		imz.cvx_code,
		imz.manufacturer,
		imz.lot_number,
		imz.administered_by_id,
		imz.education_date,
		imz.vis_date,
		imz.note,
		imz.created_by,
		imz.create_date,
		imz.updated_by,
		imz.amount_administered,
		imz.amount_administered_unit,
		imz.expiration_date,
		imz.route,
		imz.administration_site,
		imz.completion_status,
		imz.information_source,
		imz.ordering_provider,
		imz.refusal_reason,
		codes.code_text AS  cvx_code_text,
		lo.title AS manufacturer_name,
		lo1.title AS administration_site_name,
		lo2.title AS completion_status_name,
		lo3.title AS information_source_name,
		lo4.title AS route_name,
		patient.genericname1 as patient_name
FROM openemr.immunizations AS imz 
LEFT JOIN codes ON imz.cvx_code = codes.code
LEFT JOIN list_options AS lo
      ON lo.list_id = 'Immunization_Manufacturer' AND imz.manufacturer = lo.option_id AND lo.activity = 1
LEFT JOIN list_options AS lo1
      ON lo1.list_id = 'immunization_administered_site' AND imz.administration_site = lo1.option_id AND lo.activity = 1
LEFT JOIN list_options AS lo2
      ON lo2.list_id = 'Immunization_Completion_Status' AND imz.completion_status = lo2.option_id AND lo.activity = 1
LEFT JOIN list_options AS lo3
      ON lo3.list_id = 'immunization_informationsource' AND imz.information_source = lo3.option_id AND lo.activity = 1
LEFT JOIN list_options AS lo4
      ON lo4.list_id = 'drug_route' AND imz.route = lo4.option_id AND lo.activity = 1
LEFT JOIN patient_data as patient ON patient.pid = imz.patient_id
WHERE imz.id = ?;`;

    return {query, params: [immunizationId]};
}

function mapImmunizationResultToImmunizationFHIRResource(immunization){
    let fhirResource = {};
    fhirResource.identifier = [
        {use: 'usual', system: 'https://www.open-emr.org', value: immunization.id },
    ];

    if (immunization.completion_status === 'Completed'){
        fhirResource.status = immunization.completion_status;
	}
	else {
        fhirResource.notGiven = true;
	}

	fhirResource.vaccineCode = {
        'coding': [
            {
                'system': 'http://hl7.org/fhir/sid/cvx',
                'code': immunization.cvx_code
            }
        ],
            'text': immunization.cvx_code_text
    };

    fhirResource.patient = {
        'id': immunization.patient_id,
        'reference': `Patient/${immunization.patient_id}`,
        'display': immunization.patient_name
    };

    fhirResource.date = immunization.administered_date;
    fhirResource.manufacturer = {
        'id': immunization.manufacturer,
        'reference': `Organization/${immunization.manufacturer}`,
        'display': immunization.manufacturer_name
    };

    fhirResource.lotNumber = immunization.lot_number;
    fhirResource.expirationDate = immunization.expiration_date;

    fhirResource.site = {
        text: immunization.administration_site
    };

    fhirResource.route = {
    	text: immunization.route_name
	};

    fhirResource.doseQuantity = {
        'value': immunization.amount_administered,
		'system': 'http://unitsofmeasure.org',
		'code': immunization.amount_administered_unit
    };

    fhirResource.practitioner = [
        {
            'actor': {
                'reference': `Practitioner/${immunization.ordering_provider}`
            }
        }];

    fhirResource.note = [
        {
            'text': immunization.note
        }
        ];

	fhirResource.explanation = {
        'reasonNotGiven': [
            {
                text: immunization.refusal_reason
            }
        ]
    };

    fhirResource.meta = {versionId: 1, lastUpdated: immunization.create_date};

    return fhirResource;
}

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> searchById');

	let { base_version, id } = args;

	let Immunization = getImmunization(base_version);

	let connection = globals.get(CLIENT);
    let {query, params} = getImmunizationWithIdQuery(id);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Observation.searchById: ', err);
            reject(err);
        }
        else {
            if (results[0]) {
                let conditionResult = mapImmunizationResultToImmunizationFHIRResource(results[0]);
                resolve(new Immunization(conditionResult));
            }
        }
        resolve();
    });
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> create');

	let { base_version, id, resource } = args;

	let Immunization = getImmunization(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to Immunization Class
	let immunization_resource = new Immunization(resource);
	immunization_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: immunization_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> update');

	let { base_version, id, resource } = args;

	let Immunization = getImmunization(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to Immunization Class
	let immunization_resource = new Immunization(resource);
	immunization_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: immunization_resource.id, created: false, resource_version: immunization_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let Immunization = getImmunization(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to Immunization Class
	let immunization_resource = new Immunization();

	// Return resource class
	resolve(immunization_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let date = args['date'];
	let dose_sequence = args['dose-sequence'];
	let identifier = args['identifier'];
	let location = args['location'];
	let lot_number = args['lot-number'];
	let manufacturer = args['manufacturer'];
	let notgiven = args['notgiven'];
	let patient = args['patient'];
	let practitioner = args['practitioner'];
	let reaction = args['reaction'];
	let reaction_date = args['reaction-date'];
	let reason = args['reason'];
	let reason_not_given = args['reason-not-given'];
	let status = args['status'];
	let vaccine_code = args['vaccine-code'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Immunization = getImmunization(base_version);

	// Cast all results to Immunization Class
	let immunization_resource = new Immunization();

	// Return Array
	resolve([immunization_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Immunization >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let date = args['date'];
	let dose_sequence = args['dose-sequence'];
	let identifier = args['identifier'];
	let location = args['location'];
	let lot_number = args['lot-number'];
	let manufacturer = args['manufacturer'];
	let notgiven = args['notgiven'];
	let patient = args['patient'];
	let practitioner = args['practitioner'];
	let reaction = args['reaction'];
	let reaction_date = args['reaction-date'];
	let reason = args['reason'];
	let reason_not_given = args['reason-not-given'];
	let status = args['status'];
	let vaccine_code = args['vaccine-code'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Immunization = getImmunization(base_version);

	// Cast all results to Immunization Class
	let immunization_resource = new Immunization();

	// Return Array
	resolve([immunization_resource]);
});

