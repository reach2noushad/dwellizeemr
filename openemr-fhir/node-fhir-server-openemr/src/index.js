const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const asyncHandler = require('./lib/async-handler');
const mysqlClient = require('./lib/mysql');
const globals = require('./globals');

const {
	fhirServerConfig,
    mySqlConfig
} = require('./config');

const {
	CLIENT,
	CLIENT_DB
} = require('./constants');

let main = async function () {

	// Connect to mysql and pass any options here
	let [ mySQLErr, client ] = await asyncHandler(
        mysqlClient(mySqlConfig)
	);

	if (mySQLErr) {
		console.error(mySQLErr);
		process.exit(1);
	}

	// Save the client in another module so I can use it in my services
	globals.set(CLIENT, client);
	globals.set(CLIENT_DB, mySqlConfig.database);


	// Start our FHIR server
	let server = FHIRServer.initialize(fhirServerConfig);
	server.listen(fhirServerConfig.server.port, () => server.logger.verbose('Server is up and running!'));
};

main();
