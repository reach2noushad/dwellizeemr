let mysql = require('mysql');

/**
 * @name connect
 * @summary Connect to MySQL
 * @param {Object} options -Connection details
 * @return {Promise}
 */
let connect = ( options) => new Promise((resolve, reject) => {
	try {
        const connection = mysql.createConnection({
            host: options.host,
            user: options.user,
            password: options.password,
            database: options.database
        });

        resolve(connection);
    } catch (err){
		reject(err);
	}

});


module.exports = connect;
