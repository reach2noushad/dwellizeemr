ALTER TABLE `openemr`.`procedure_order_code`
ADD COLUMN `specimen-code` VARCHAR(45) NULL AFTER `procedure_order_title`,
ADD COLUMN `specimen-name` VARCHAR(100) NULL AFTER `specimen-code`;

ALTER TABLE `openemr`.`procedure_order_code`
ADD COLUMN `reason` VARCHAR(150) NULL AFTER `specimen_name`;
