<?php

/**
 * @package OpenEMR
 * @author Rabeesh MP<rabeesh.mp@attinadsoftware.com>
 * @link    http://www.open-emr.org
 */
require_once dirname(__FILE__) . '/base_controller.php';
require_once("{$GLOBALS['srcdir']}/appointments.inc.php");
require_once("{$GLOBALS['srcdir']}/pid.inc");

class AttinadsGroupController extends BaseController {

    public $attinadsGroupModel;

    /* Note: Created functions to return arrays so that xl method can be used in array rendering. */

    //list of group statuses
    public static function prepareStatusesList() {
        $statuses = array(
            '10' => xl('Active'),
            '20' => xl('Finished'),
            '30' => xl('Canceled')
        );
        return $statuses;
    }

    //list of group types
    public static function prepareGroupTypesList() {
        $group_types = array(
            '1' => xl('Closed'),
            '2' => xl('Open'),
            '3' => xl('Training')
        );
        return $group_types;
    }

    //Max length of notes preview in groups list
    private $notes_preview_proper_length = 30;

    /**
     * add / edit attinads group
     * making validation and saving in the match tables.
     * @param null $groupId - must pass when edit group
     */
    public function index($groupId = null) {

        $data = array();
        if ($groupId) {
            self::setSession($groupId);
        }

        //Load models
        $this->attinadsGroupModel = $this->loadModel('attinads_groups');
        $userModel = $this->loadModel('Users');
        //Get users
        $users = $userModel->getAllUsers();
        $data['users'] = $users;

        //Get statuses
        $data['statuses'] = self::prepareStatusesList();



        if (isset($_POST['save'])) {
            $_POST['group_start_date'] = DateToYYYYMMDD($_POST['group_start_date']);
            $_POST['group_end_date'] = DateToYYYYMMDD($_POST['group_end_date']);
            $isEdit = empty($_POST['group_id']) ? false : true;

            // for new group - checking if already exist same name
            if ($_POST['save'] != 'save_anyway' && $this->alreadyExist($_POST, $isEdit)) {
                $data['message'] = xlt('Failed - already has group with the same name') . '.';
                $data['savingStatus'] = 'exist';
                $data['groupData'] = $_POST;
                if ($isEdit) {
                    $this->loadView('groupDetailsGeneralData', $data);
                } else {
                    $this->loadView('addGroup', $data);
                }
            }

            $filters = array(
                'group_name' => FILTER_DEFAULT,
                'group_start_date' => FILTER_SANITIZE_SPECIAL_CHARS,
                'group_type' => FILTER_VALIDATE_INT,
                'group_participation' => FILTER_VALIDATE_INT,
                'group_status' => FILTER_VALIDATE_INT,
                'group_notes' => FILTER_DEFAULT,
                'group_guest_counselors' => FILTER_DEFAULT,
                'counselors' => array('filter' => FILTER_VALIDATE_INT,
                    'flags' => FILTER_FORCE_ARRAY)
            );
            if ($isEdit) {
                $filters['group_end_date'] = FILTER_SANITIZE_SPECIAL_CHARS;
                $filters['group_id'] = FILTER_VALIDATE_INT;
            }

            //filter and sanitize all post data.
            $data['groupData'] = filter_var_array($_POST, $filters);
            if (!$data['groupData']) {
                $data['message'] = xlt('Failed to create new group') . '.';
                $data['savingStatus'] = 'failed';
            } else {
                if (!$isEdit) {
                    // save new group
                    $id = $this->saveNewGroup($data['groupData']);
                    $data['groupData']['group_id'] = $id;
                    $data['message'] = xlt('New group was saved successfully') . '.';
                    $data['savingStatus'] = 'success';
                    self::setSession($id);
                    //$events = $eventsModel->getGroupEvents($id);
                    //$data['events'] = $events;
                    $data['readonly'] = 'disabled';

                    $this->loadView('groupDetailsGeneralData', $data);
                } else {
                    //update group
                    $this->updateGroup($data['groupData']);
                    $data['message'] = xlt("Detail's group was saved successfully") . '.';
                    $data['savingStatus'] = 'success';
                    $data['readonly'] = 'disabled';
                    $this->loadView('groupDetailsGeneralData', $data);
                }
            }

            // before saving
        } else {
            //for new form
            $data['groupData'] = array('group_name' => null,
                'group_start_date' => date('Y-m-d'),
                'group_type' => null,
                'group_participation' => null,
                'group_notes' => null,
                'group_guest_counselors' => null,
                'group_status' => null
            );
            $this->loadView('addGroup', $data);
        }
    }

    /**
     * check if exist group with the same name and same start date
     * @param $groupData
     * @param $isEdit type of testing
     * @return bool
     */
    private function alreadyExist($groupData, $isEdit = false) {

        if ($isEdit) {
            //return false if not touched on name and date
            $databaseData = $this->attinadsGroupModel->getGroup($groupData['group_id']);
            if ($databaseData['group_name'] == $groupData['group_name'] && $databaseData['group_start_date'] == $groupData['group_start_date']) {
                return false;
            }
        }

        $isExistGroup = $this->attinadsGroupModel->existGroup($groupData['group_name'], $groupData['group_start_date'], $isEdit ? $groupData['group_id'] : null);
        //true / false
        return $isExistGroup;
    }

    /**
     * Controller for loading the attinads groups to be listed in 'listGroups' view.
     */
    public function listGroups() {

        //If deleting a group
        if ($_GET['deleteGroup'] == 1) {
            $group_id = $_GET['group_id'];
            $deletion_response = $this->deleteGroup($group_id);
            $data['deletion_try'] = 1;
            $data['deletion_response'] = $deletion_response;
        }

        //Load attinads groups from DB.
        $attinads_groups_model = $this->loadModel('Attinads_Groups');
        $attinads_groups = $attinads_groups_model->getAllGroups();
        //print_r($attinads_groups); exit;
        //Load counselors from DB.
        $counselors_model = $this->loadModel('Attinads_Groups_Counselors');
        $counselors = $counselors_model->getAllCounselors();

        //Merge counselors with matching groups and prepare array for view.
        $data['attinadsGroups'] = $this->prepareGroups($attinads_groups, $counselors);

        //Insert static arrays to send to view.
        $data['statuses'] = self::prepareStatusesList();
        $data['group_types'] = self::prepareGroupTypesList();
        $data['group_participation'] = self::prepareGroupParticipationList();
        $data['counselors'] = $this->prepareCounselorsList($counselors);
        //print_r($data); exit;
        //Send groups array to view.
        $this->loadView('listGroups', $data);
    }

    /**
     * Prepares the attinads group list that will be sent to view.
     * @param $attinads_groups
     * @param $counselors
     * @return array
     */
    private function prepareGroups($attinads_groups, $counselors) {

        $new_array = array();
        $users_model = $this->loadModel('Users');

        //Insert groups into a new array and shorten notes for preview in list
        foreach ($attinads_groups as $attinads_group) {
            $gid = $attinads_group['group_id'];
            $new_array[$gid] = $attinads_group;
            $new_array[$gid]['group_notes'] = $this->shortenNotes($attinads_group['group_notes']);
            $new_array[$gid]['counselors'] = array();
        }

        //Insert the counselors into their groups in new array.
        foreach ($counselors as $counselor) {
            $group_id_of_counselor = $counselor['group_id'];
            $counselor_id = $counselor['user_id'];
            $counselor_name = $users_model->getUserNameById($counselor_id);
            if (is_array($new_array[$group_id_of_counselor])) {
                array_push($new_array[$group_id_of_counselor]['counselors'], $counselor_name);
            }
        }

        return $new_array;
    }

    private function shortenNotes($notes) {

        $length = strlen($notes);
        if ($length > $this->notes_preview_proper_length) {
            $notes = mb_substr($notes, 0, 50) . '...';
        }

        return $notes;
    }

    /**
     * Change group status to 'deleted'. Can be done only if group has no encounters.
     * @param $group_id
     * @return array
     */
    private function deleteGroup($group_id) {

        $response = array();

        //If group has encounters cannot delete the group.
        $group_has_encounters = $this->checkIfHasApptOrEncounter($group_id);
        if ($group_has_encounters) {
            $response['success'] = 0;
            $response['message'] = xl("Deletion failed because group has appointments or encounters");
        } else {
            //Change group status to 'canceled'.
            $attinads_groups_model = $this->loadModel('Attinads_Groups');
            $attinads_groups_model->changeGroupStatus($group_id, 30);
            $response['success'] = 1;
        }

        return $response;
    }

    /**
     * Checks if group has upcoming  appointments or encounters
     * @param $group_id
     * @return bool
     */
    private function checkIfHasApptOrEncounter($group_id) {
        $attinads_groups_events_model = $this->loadModel('Attinads_Groups_Events');
        $attinads_groups_encounters_model = $this->loadModel('Attinads_Groups_Encounters');
        $events = $attinads_groups_events_model->getGroupEvents($group_id);
        $encounters = $attinads_groups_encounters_model->getGroupEncounters($group_id);
        if (empty($events) && empty($encounters)) {
            return false; //no appts or encounters so can delete
        }

        return true; //appts or encounters exist so can't delete
    }

    /**
     * insert a new group to attinads_group table and connect between user-counselor to group at attinads_Groups_Counselors table
     * @param $groupData
     * @return int $groupId
     */
    private function saveNewGroup($groupData) {
        //print_r($groupData); exit;
        $counselors = !empty($groupData['counselors']) ? $groupData['counselors'] : array();
        unset($groupData['groupId'], $groupData['save'], $groupData['counselors']);

        $groupId = $this->attinadsGroupModel->saveNewGroup($groupData);

        foreach ($counselors as $counselorId) {
            $this->counselorsModel->save($groupId, $counselorId);
        }

        return $groupId;
    }

    /**
     * update group in attinads_group table and the connection between user-counselor to group at attinads_Groups_Counselors table
     * @param $groupData
     * @return int $groupId
     */
    private function updateGroup($groupData) {

        $counselors = !empty($groupData['counselors']) ? $groupData['counselors'] : array();
        unset($groupData['save'], $groupData['counselors']);

        $this->attinadsGroupModel->updateGroup($groupData);

        $this->counselorsModel->remove($groupData['group_id']);
        foreach ($counselors as $counselorId) {
            $this->counselorsModel->save($groupData['group_id'], $counselorId);
        }
    }
    //list of participation types
    public static function prepareGroupParticipationList()
    {
        $group_participation = array(
            '1' => xl('Mandatory'),
            '2' => xl('Optional')
        );
        return $group_participation;
    }
/**
     * Returns a list of counselors without duplicates.
     * @param $counselors
     * @return array
     */
    private function prepareCounselorsList($counselors)
    {
        $new_array = array();
        $users_model = $this->loadModel('Users');

        foreach ($counselors as $counselor) {
            $counselor_id = $counselor['user_id'];
            $counselor_name = $users_model->getUserNameById($counselor_id);
            $new_array[$counselor_id] = $counselor_name;
        }

        return $new_array;
    }
    static function setSession($groupId) {

        setpid(0);
        if ($_SESSION['attinads_group'] != $groupId) {
            $_SESSION['attinads_group'] = $groupId;
        }
    }

}
