<?php
/**
 * find_code_popup.php
 *
 * @package   OpenEMR
 * @link      http://www.open-emr.org
 * @author    Rod Roark <rod@sunsetsystems.com>
 * @author    Brady Miller <brady.g.miller@gmail.com>
 * @copyright Copyright (c) 2008-2014 Rod Roark <rod@sunsetsystems.com>
 * @copyright Copyright (c) 2018 Brady Miller <brady.g.miller@gmail.com>
 * @license   https://github.com/openemr/openemr/blob/master/LICENSE GNU General Public License 3
 */


require_once('../../globals.php');
require_once($GLOBALS['srcdir'].'/patient.inc');
require_once($GLOBALS['srcdir'].'/csv_like_join.php');
require_once($GLOBALS['fileroot'].'/custom/code_types.inc.php');

if (!empty($_POST)) {
    if (!verifyCsrfToken($_POST["csrf_token_form"])) {
        csrfNotVerified();
    }
}

$info_msg = "";
$codetype = $_REQUEST['codetype'];
if (!empty($codetype)) {
    $allowed_codes = split_csv_line($codetype);
}

$form_code_type = $_POST['form_code_type'];

// Determine which code type will be selected by default.
$default = '';
if (!empty($form_code_type)) {
    $default = $form_code_type;
} else if (!empty($allowed_codes) && count($allowed_codes) == 1) {
    $default = $allowed_codes[0];
} else if (!empty($_REQUEST['default'])) {
    $default = $_REQUEST['default'];
}

// This variable is used to store the html element
// of the target script where the selected code
// will be stored in.
$target_element = $_GET['target_element'];
?>
<html>
<head>
<?php html_header_show(); ?>
<title><?php echo xlt('Code Finder'); ?></title>
<link rel="stylesheet" href='<?php echo $css_header ?>' type='text/css'>

<style>
td { font-size:10pt; }
</style>
<script type="text/javascript" src="<?php echo $webroot ?>/interface/main/tabs/js/include_opener.js"></script>
<script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative']; ?>/jquery/dist/jquery.min.js"></script>  

<script language="JavaScript">

 // Standard function
 function selcode(codetype, code, selector, codedesc) {
  if (opener.closed || ! opener.set_related) {
   alert(<?php echo xlj('The destination form was closed; I cannot act on your selection.'); ?>);
  }
  else {
   var msg = opener.set_related_bodysite(codetype, code, selector, codedesc);
   if (msg) alert(msg);
      dlgclose();
   return false;
  }
 }

 // TBD: The following function is not necessary. See
 // interface/forms/LBF/new.php for an alternative method that does not require it.
 // Rod 2014-04-15

 // Standard function with additional parameter to select which
 // element on the target page to place the selected code into.
 function selcode_target(codetype, code, selector, codedesc, target_element) {
  if (opener.closed || ! opener.set_related_target)
   alert(<?php echo xlj('The destination form was closed; I cannot act on your selection.'); ?>);
  else
   opener.set_related_target(codetype, code, selector, codedesc, target_element);
     dlgclose();
  return false;
 }

</script>

</head>
<?php 
    $focus = "document.theform.search_term.select();";
?>
<body class="body_top" OnLoad="<?php echo $focus; ?>">

<?php
$string_target_element = "";
if (!empty($target_element)) {
    $string_target_element = "?target_element=" . attr_url($target_element) . "&";
} else {
    $string_target_element = "?";
}
?>
<?php if (!empty($allowed_codes)) {?>
  <form method='post' name='theform' action='find_code_bodysite_popup.php<?php echo $string_target_element ?>codetype=<?php echo attr_url($codetype) ?>'>
<?php } else { ?>
  <form method='post' name='theform' action='find_code_bodysite_popup.php<?php echo $string_target_element ?>'>
<?php } ?>
  <input type="hidden" name="csrf_token_form" value="<?php echo attr(collectCsrfToken()); ?>" />

<center>

<table border='0' cellpadding='5' cellspacing='0'>

 <tr>
  <td height="1">
  </td>
 </tr>

 <tr class = "head" bgcolor='#ddddff'>
  <td>
   <b>

<?php $allowed_codes = '';
if (!empty($allowed_codes)) {
    if (count($allowed_codes) === 1) {
        echo "<input type='text' name='form_code_type' value='" . attr($codetype) . "' size='5' readonly>\n";
    } else {
?>
   <select name='form_code_type'>
<?php
foreach ($allowed_codes as $code) {
    $selected_attr = ($default == $code) ? " selected='selected'" : '';
?>
<option value='<?php echo attr($code) ?>'<?php echo $selected_attr ?>><?php echo xlt($code_types[$code]['label']) ?></option>
<?php
}
?>
   </select>
<?php
    }
} else {
  // No allowed types were specified, so show all.
    echo "   <select name='form_code_type'";
    echo ">\n";
    foreach ($code_types as $key => $value) {
        echo "    <option value='" . attr($key) . "'";
        if ($default == $key) {
            echo " selected";
        }

        echo ">" . xlt($value['label']) . "</option>\n";
    }

    echo "    <option value='PROD'";
    if ($default == 'PROD') {
        echo " selected";
    }

    echo ">" . xlt("Product") . "</option>\n";
    echo "   </select>&nbsp;&nbsp;\n";
}
?>

    <!-- <?php echo xlt('Search for:'); ?> -->
   <input type='hidden' name='search_term' size='12' value='//<?php echo attr($_REQUEST['search_term']); ?>'
    title='<?php echo xla('Any part of the desired code or its description'); ?>' />
   &nbsp;
   <input type='submit' name='bn_search' value='<?php echo xla('Search'); ?>' />
   &nbsp;&nbsp;&nbsp;
    <?php if (!empty($target_element)) { ?>
     <input type='button' value='<?php echo xla('Erase'); ?>' onclick="selcode_target('', '', '', '', <?php echo attr_js($target_element); ?>)" />
    <?php } else { ?>
     <input type='button' value='<?php echo xla('Erase'); ?>' onclick="selcode('', '', '', '')" />
    <?php } ?>
   </b>
  </td>
 </tr>

 <tr>
  <td height="1">
  </td>
 </tr>

</table>
    
<?php
if (($_REQUEST['bn_search'] || $_REQUEST['search_term']) && $form_code_type == "SNOMED-CT") {
    if (!$form_code_type) {
        $form_code_type = $codetype;
    }
?>

<table border='0' id="observation-tb" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
     <thead>
        <tr>
            <th><b><?php echo xlt('Code'); ?></b></th>
            <th><b><?php echo xlt('Description'); ?></b></th>
        </tr>
    </thead>
    <tr>
        <td></td>
        <td></td>
    </tr>
</table>

<?php } ?>
</center>
</form>     
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script language="JavaScript">
        $(document ).ready(function() {
                $('#observation-tb').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax":{
                       url :"server_processing_bodysite.php", // json datasource
                       type: "post",  // type of method  , by default would be get
                       error: function(result){
                           console.log("error");
                       }
                     }
                });   
        });
    </script>
</body>
</html>
