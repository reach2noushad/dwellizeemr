<?php
    require_once("../../globals.php");
    require_once("../../../custom/code_types.inc.php");
    if(isset($_POST['studyResult'])){
        $studyResults = $_POST['studyResult'];
        $studyId = $_POST['studyId'];
?>
        <style type="text/css">
            .con-tab {
                padding-top: .5em;
            }            
        </style>
        <table class="table table-striped table-bordered table-condensed tablesorter treeTable hasSaveSort" style="width: 98%;" id="treeTable">
            <thead>
                <tr>
                    <th data-column="0" class="header"><div class="tablesorter-header-inner">Study Id</div></th>
                    <th data-column="1" class="header"><div class="tablesorter-header-inner">Study Instance UID</div></th>
                    <th data-column="2" class="header"><div class="tablesorter-header-inner">Study Date</div></th>
                    <th data-column="3" class="header"><div class="tablesorter-header-inner">Study TIme</div></th>
                    <th data-column="4" class="header"><div class="tablesorter-header-inner">Accession Number</div></th>                                            
                    <th data-column="6" class="header"><div class="tablesorter-header-inner">Study Description</div></th>
                    <th data-column="7" class="header"><div class="tablesorter-header-inner">Reason Code</div></th>
                    <th data-column="8" class="header"><div class="tablesorter-header-inner">Reason</div></th>
                </tr>
            </thead>
            <tbody>                                                          
                <?php foreach($studyResults as $key=>$value){ ?>
                    <?php if($studyId==$key){ $id = $value[2];?>
                        <tr>                                            
                            <td><?php echo $value[2]; ?></td>
                            <td><?php echo $key; ?></td>
                            <td><?php echo $value[1]; ?></td>
                            <td><?php echo $value[3]; ?></td>
                            <td><?php echo $value[4]; ?></td>
                            <td><?php echo $value[5]; ?></td>
                            <td><?php echo $value[6]; ?></td>
                            <td><?php echo $value[7]; ?></td>
                        </tr> 
                    <?php } ?>             
                <?php } ?>                                                                              
            </tbody>
        </table>
        <form method='post' action=''>
            <div class="col-sm-9">
               <table>
                   <tr>
                       <td class="con-tab"><label for="conclusion" class="control-label text-right">Conclusion</label></td>
                       <td class="con-tab"><textarea name="conclusion" class="form-control" title="Add Conclusion" rows="4" cols="35"></textarea></td>
                   </tr>
                    <tr>
                       <td class="con-tab"><label for="conclusion_code" class="control-label text-right">Conclusion Code</label></td>
                       <td class="con-tab"><input class="form-control" type="text" name="form_proc_type_diag" onclick="sel_related(this.name)" title="Click to add conclusion code" onfocus="this.blur()" style="cursor:pointer;cursor:hand" readonly=""></td>
                       <input type="hidden" name="conclusion_code_desc" id="conclusion_code_desc">
                       <input type="hidden" name="study_id" id="study_id" value="<?php echo $id; ?>">
                   </tr>
                </table>
                <center>
                    <p><input type='submit' name='form_submit' value='<?php xl('Sign Results', 'e'); ?>' /></p>
                </center>
           </div>            
        </form>
        <script language="javascript" type="text/javascript">
                // This invokes the find-code popup.
                function sel_related(varname) {
                    rcvarname = varname;
                    // codetype is just to make things easier and avoid mistakes.
                    // Might be nice to have a lab parameter for acceptable code types.
                    // Also note the controlling script here runs from interface/patient_file/encounter/.
                    let title = '<?php echo "Select Diagnosis Codes"; ?>';
                    var url = '<?php echo $rootdir ?>/patient_file/encounter/find_conclusion_code_image_dynamic.php';
                    dlgopen(url+'?codetype=<?php echo attr(collect_codetypes("diagnosis", "csv")); ?>', '_blank', 985, 750, '', title);
                }


                // This is for callback by the find-code popup.
                // Returns the array of currently selected codes with each element in codetype:code format.
                function get_related() {
                    return document.forms[0][rcvarname].value.split(';');
                }

                // This is for callback by the find-code popup.
                // Appends to or erases the current list of related codes.
                function set_related(codetype, code, selector, codedesc) {   
                    var f = document.forms[0];
                    var s = f[rcvarname].value;
                    var descId = document.getElementById('conclusion_code_desc');
                    var temp = descId.value;
                    if(temp==""){
                        descId.value = codedesc;
                    }else{
                        //descId.value = temp+";"+codedesc;
                        descId.value = codedesc;
                    }    
                    if(codetype == 'SNOMED'){
                        codetype = codetype+"-CT";
                    }
                    if (code) {
                        if (s.length > 0) s += ';';
                        //s += codetype + ':' + code;
                         s = codetype + ':' + code;
                    } else {
                        s = '';
                    }
                    f[rcvarname].value = s;    
                }
        </script>
<?php }elseif(isset($_POST['seriesResult'])){
        $seriesResults = $_POST['seriesResult'];
        $seriesId = $_POST['seriesId'];
?>
        <table class="table table-striped table-bordered table-condensed tablesorter treeTable hasSaveSort" style="width: 98%;" id="treeTable">
            <thead>
                <tr>
                    <th data-column="0" class="header"><div class="tablesorter-header-inner">Series Instance id</div></th>
                    <th data-column="1" class="header"><div class="tablesorter-header-inner">Series Modality</div></th>
                    <th data-column="2" class="header"><div class="tablesorter-header-inner">Series Description</div></th>
                    <th data-column="3" class="header"><div class="tablesorter-header-inner">Series Date</div></th>
                    <th data-column="4" class="header"><div class="tablesorter-header-inner">Series Time</div></th>
                    <th data-column="5" class="header"><div class="tablesorter-header-inner">Body Site</div></th>
                    <th data-column="6" class="header"><div class="tablesorter-header-inner">Body Site Code</div></th>
                </tr>
            </thead>
            <tbody>                                                          
                <?php foreach($seriesResults as $key=>$value){ ?>
                    <?php if($seriesId==$key){?>
                        <tr>                                            
                            <td><?php echo $key; ?></td>
                            <td><?php echo $value[0]; ?></td>
                            <td><?php echo $value[1]; ?></td>
                            <td><?php echo $value[2]; ?></td>
                            <td><?php echo $value[3]; ?></td>
                            <td><?php echo $value[4]; ?></td>
                            <td><?php echo $value[5]; ?></td>
                        </tr> 
                    <?php } ?>             
                <?php } ?>                                                                              
            </tbody>
        </table>
    
<?php }elseif(isset($_POST['instanceResult'])){ 
        $instanceResults = $_POST['instanceResult'];
        $instanceId = $_POST['instanceId'];
        $inputUrl = "http://192.168.11.13:8082/dcm4chee-arc/aets/DCM4CHEE/wado?requestType=WADO&contentType=application/dicom&transferSyntax=*";
        $url = "http://192.168.2.27:8082/?input=";
?>
        <table class="table table-striped table-bordered table-condensed tablesorter treeTable hasSaveSort" style="width: 98%;" id="treeTable">
            <thead>
                <tr>
                    <th data-column="0" class="header"><div class="tablesorter-header-inner">Instance uid</div></th>
                    <th data-column="1" class="header"><div class="tablesorter-header-inner">Instance Number</div></th>                    
                </tr>
            </thead>
            <tbody>                                                          
                <?php foreach($instanceResults as $key=>$value){ ?>
                    <?php if($instanceId==$key){?>
                        <?php $inputUrl = urlencode('http://192.168.11.13:8082/dcm4chee-arc/aets/DCM4CHEE/wado?requestType=WADO&contentType=application/dicom&transferSyntax=*'.'&studyUID='.$value[3].'&seriesUID='.$value[4].'&objectUID='.$key);?>
                        <tr>                                            
                            <td><?php echo '<a href="'.$url.$inputUrl.'" target="_blank">'.$key.'</a>'; ?></td>
                            <td><?php echo $value[0]; ?></td>
                        </tr> 
                    <?php } ?>             
                <?php } ?>                                                                              
            </tbody>
        </table>
<?php } ?>



